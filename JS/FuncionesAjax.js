function Buscar() {

    var cmd = document.getElementById("regiones").value;
    var xmlhttp = new XMLHttpRequest();
    
    xmlhttp.onreadystatechange = function () {
       if (this.readyState === 4 && this.status === 200) {
 
    var respuesta = JSON.parse(this.response);
    var html = '';
    var i;
    if(respuesta.data.length == 0){
        var html='';
        $('#table-contenido').html(html);
        document.getElementById('btnExportar').style.display = 'none';
        alert("No existe data para mostrar");
    }else{
        document.getElementById('btnExportar').style.display = '';

        for (i = 0; i < respuesta.data.length; i++) {
           var latitud="'"+respuesta.data[i].latitud+"'";
           var longitud = "'"+respuesta.data[i].longitud+"'";
           var comunidad = "'Comunidad "+respuesta.data[i].comunidad+"'";
            html += '<tr>' +
                '<td>' + i + '</td>' +
                '<td>' + respuesta.data[i].nombre + '</td>' +
                '<td>' + respuesta.data[i].id + '</td>' +
                '<td>' + respuesta.data[i].region + '</td>' +
                '<td>' + respuesta.data[i].comuna + '</td>' +
                '<td>' + respuesta.data[i].comunidad + '</td>' +
                '<td>' + respuesta.data[i].latitud + '</td>' +
                '<td>' + respuesta.data[i].longitud + '</td>' 
                +'<td><input id="btnMap" onclick="inicializar('+latitud+','+longitud+','+comunidad+');" type="button" class="btn btn-info" data-toggle="modal" data-target="#miModal2" value="Ver Mapa"></td>' +
                '</tr>';
            }
            $('#table-contenido').html(html);
            document.getElementById('btnExportar1').style.display = '';
    
              
           }
    }
    
    };
    xmlhttp.open("GET", "CONTROLLER/consumirApi.php?cmd=" + cmd, true);
    xmlhttp.send();
 }
 function validarCoordenadas() {
    var lat=document.getElementById("latitud").value; 
    var long=document.getElementById("Longitud").value;
    var coordenadas =lat+", "+long;
    var name=document.getElementById("name").value;
    var region=document.getElementById("Regiones").value; 
    var comuna=document.getElementById("comunas").value; 
    var comunidad=document.getElementById("comunidad").value; 
    if(name=="" || region=="" || comuna=="" || comunidad=="" || lat == "" || long==""){
        alert("Debe ingresar todos los campos");
    }else if (!coordenadas.match(/^[-]?\d+[\.]?\d*, [-]?\d+[\.]?\d*$/)) {
        alert("Formato para latitud o longitud incorrecta");
    }else{
        IngresarMesa(name,region,comuna,comunidad,lat,long);
    }
}
 function IngresarMesa(name,region,comuna,comunidad,lat,long){
    var cmd = "ingresoMesa";
    var id = document.getElementById("id").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {

            if (this.readyState === 4 && this.status === 200) {
                alert(this.responseText);
                location.reload();
            }
        }
    };
    xmlhttp.open("GET", "CONTROLLER/controlador.php?cmd=" + cmd + 
    "&nombre=" + name +
    "&region=" + region+
    "&comuna=" + comuna +
    "&comunidad=" + comunidad +
    "&latitud=" + lat +
    "&longitud=" + long +
    "&id="+id);
    xmlhttp.send();

    
 }
 function PasarInformacion(id) {
    
    var id = id;
    var cmd = "pasardata";
    var xmlhttp = new XMLHttpRequest();
    
    xmlhttp.onreadystatechange = function () {
       if (this.readyState === 4 && this.status === 200) {
          var respuesta = JSON.parse(this.response);
          document.getElementById("name").value= respuesta.nombre;
          document.getElementById("regiones").value = respuesta.region;
          document.getElementById("comunas").value = respuesta.comuna;
          document.getElementById("comunidad").value = respuesta.comunidad;
          document.getElementById("latitud").value = respuesta.latitud;
          document.getElementById("Longitud").value = respuesta.longitud;
          document.getElementById("id").value = respuesta.id;
       }
    };
    xmlhttp.open("GET", "CONTROLLER/controlador.php?cmd=" + cmd + "&id=" + id, true);
    xmlhttp.send();
 }
 function Eliminar(id){
    var id = id;
   var pregunta = confirm("Esta Seguro que desea eliminar esta mesa de trabajo?");
   if (pregunta == true) {
      var cmd = "EliminarMesa";
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function () {
         if (this.readyState === 4 && this.status === 200) {
            alert(this.responseText);
            location.reload();
         }
      };
      xmlhttp.open("GET", "CONTROLLER/controlador.php?cmd=" + cmd + "&id=" + id, true);
      xmlhttp.send();
   } else {
      alert("Operación Cancelada");
   }
 }
 function fnExcelReport(tabla)
{
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    tab = document.getElementById(tabla); // id of table

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
    }  
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    return (sa);
}
function inicializar(lat,long,name) {
   //Opciones del mapa
   var OpcionesMapa = {
       center: new google.maps.LatLng(lat, long),
       mapTypeId: google.maps.MapTypeId.SATELLITE, //ROADMAP  SATELLITE HYBRID TERRAIN
       zoom: 16
   };

   var miMapa;
   //constructor
   miMapa = new google.maps.Map(document.getElementById('modalMapa'), OpcionesMapa);

   //Añadimos el marcador
   var Marcador = new google.maps.Marker({
                   position: new google.maps.LatLng(38.3489719, -0.4780289000000266),
                   map: miMapa,
                   title: name
               });
}
 