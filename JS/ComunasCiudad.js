var Regiones = {

	"regiones": [{
			"numeroregion":"15",
			"NombreRegion": "Arica y Parinacota"
	},
		{
			"numeroregion":"1",
			"NombreRegion": "Tarapacá"
	},
		{
			"numeroregion":"2",
			"NombreRegion": "Antofagasta"
	},
		{
			"numeroregion":"3",
			"NombreRegion": "Atacama"
	},
		{
			"numeroregion":"4",
			"NombreRegion": "Coquimbo"
	},
		{
			"numeroregion":"5",
			"NombreRegion": "Valparaíso"
	},
		{
			"numeroregion":"6",
			"NombreRegion": "Región del Libertador Gral. Bernardo O’Higgins"
	},
		{
			"numeroregion":"7",
			"NombreRegion": "Región del Maule"
	},
		{
			"numeroregion":"8",
			"NombreRegion": "Región del Biobío"
	},
		{
			"numeroregion":"9",
			"NombreRegion": "Región de la Araucanía"
	},
		{
			"numeroregion":"14",
			"NombreRegion": "Región de Los Ríos"
	},
		{
			"numeroregion":"10",
			"NombreRegion": "Región de Los Lagos"
	},
		{
			"numeroregion":"11",
			"NombreRegion": "Región Aisén del Gral. Carlos Ibáñez del Campo"
	},
		{
			"numeroregion":"12",
			"NombreRegion": "Región de Magallanes y de la Antártica Chilena"
	},
		{
			"numeroregion":"13",
			"NombreRegion": "Región Metropolitana de Santiago"
	},
		{
			"numeroregion":"16",
			"NombreRegion": "Región de Ñuble"
	}]
}


jQuery(document).ready(function () {

	var iRegion = 0;
	var htmlRegion = '<option value="">Regiones</option>';

	jQuery.each(Regiones.regiones, function () {
		htmlRegion = htmlRegion + '<option value="' + Regiones.regiones[iRegion].numeroregion + '">' + Regiones.regiones[iRegion].NombreRegion + '</option>';
		iRegion++;
	});

	jQuery('#regiones').html(htmlRegion);

	jQuery('#regiones').change(function () {
		if (jQuery(this).val() == '') {
			alert('Regiones');
		}
	});

});

