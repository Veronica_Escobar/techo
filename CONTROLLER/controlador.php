<?php
    
    $cmd=$_REQUEST['cmd'];
    require_once "Conexion.php";
    switch($cmd){
        case "ingresoMesa":
            if(!empty($_GET['nombre']) 
            && !empty($_GET['region']) 
            && !empty($_GET['comuna']) 
            && !empty($_GET['comunidad']) 
            && !empty($_GET['latitud']) 
            && !empty($_GET['longitud'])){

                $id=empty($_POST['id'])?0:$_GET['id'];
                $name=$_GET['nombre'];
                $region=$_GET['region'];
                $comuna=$_GET['comuna'];
                $comunidad=$_GET['comunidad'];
                $latitud=$_GET['latitud'];
                $longitud=$_GET['longitud'];

                $sql2 = "SELECT idcomunidades FROM comunidades WHERE idcomunidades = $id";
                $result = $con->query($sql2);
                $cantidad=$result->num_rows;

                if($cantidad !=null){
                    $sql="UPDATE comunidades SET 
                    nombre='$name',
                    region='$region',
                    comuna='$comuna',
                    comunidad='$comunidad',
                    latitud='$latitud',
                    longitud='$longitud',
                    fechaingreso=curdate()
                    WHERE idcomunidades=$id";
                     if ($con->query($sql) === TRUE) {
                        echo "Mesa de trabajo actualizada con exito";
                        break;
                    } else {
                        echo "Error al Actualizar: " . $con->error;
                        break;
                    }
                }else{
                     $sql="INSERT INTO comunidades VALUE (
                        null,
                        '$name',
                        '$region',
                        '$comuna',
                        '$comunidad',
                        '$latitud',
                        '$longitud',
                        (curdate()),
                        1);";

                        if ($con->query($sql) === TRUE) {
                            echo "Mesa de trabajo guardada con exito";
                            break;
                        } else {
                            echo "Error al guardar mesa de trabajo: " . $con->error;
                            break;
                        }
                }
            }else{
                echo "Debe completar todos los campos";
            break;
            }

        
        case "pasardata":

            if (!empty($_GET['id'])) {
                $id = $_GET['id'];
                $sql = "SELECT *
                FROM comunidades 
                WHERE estado = 1 and idcomunidades ='$id'";
    
                $result = $con->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = mysqli_fetch_object($result)) {
                        $respuesta = array(
                            "id" => $row->idcomunidades,
                            "nombre" => $row->nombre,
                            "region" => $row->region,
                            "comuna"=>$row->comuna,
                            "comunidad" => $row->comunidad,
                            "latitud" => $row->latitud,
                            "longitud"=>$row->longitud
                        );
                    }
                }
                echo json_encode($respuesta);
                break;
            }
        case "EliminarMesa":
            $id = $_GET['id'];
            $consulta = "UPDATE comunidades SET 
            estado=0 WHERE idcomunidades=$id";
            if ($con->query($consulta) === TRUE) {
    
                echo "Mesa de trabajo Eliminado";
                break;
            } else {
    
                echo "Error al Eliminar: " . $con->error;
                break;
            }
    }