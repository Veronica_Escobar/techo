CREATE DATABASE techo;
use techo;

CREATE TABLE `comunidades` (
  `idcomunidades` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `region` varchar(45) DEFAULT NULL,
  `comuna` varchar(45) DEFAULT NULL,
  `comunidad` varchar(45) NOT NULL,
  `latitud` varchar(45) DEFAULT NULL,
  `longitud` varchar(45) DEFAULT NULL,
  `fechaingreso` date DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `comunidades`
  ADD PRIMARY KEY (`idcomunidades`);

ALTER TABLE `comunidades`
  MODIFY `idcomunidades` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

INSERT INTO `comunidades` (`idcomunidades`, `nombre`, `region`, `comuna`, `comunidad`, `latitud`, `longitud`, `fechaingreso`, `estado`) VALUES
(1, 'VILLA ANTIGUA', 'Región Metropolitana de Santiago', 'El Monte', 'Villa Antigua', '-33.6749895', '-70.9946978', '2020-06-12', 1);

