<!DOCTYPE html>
<html lang="es" >  
    <head>
        <title>Mesas de trabajo</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="JS/ComunasCiudad.js" type="text/javascript"></script>
        <script src="JS/ComunaCiudad.js" type="text/javascript"></script>
        <script src="JS/FuncionesAjax.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="CSS/estilos.css" media="screen" />
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyC02afJ_X1OHrcVUJWV7nYYeonGF7vMeAs"></script>
    </head>
    <body>
        <h1>Mesas de trabajo</h1>
        <hr>
        &nbsp;
        <div id="filtro">
            <label>Filtro</label>
            <div>
                <select id="regiones" class="form-control">
                    <option value='0'>Seleccione</option>
                </select>
                <input id="btnBuscar" type="button" onclick="Buscar();" class="btn btn-info" value="Buscar">
                <div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Agregar mesa de trabajo</h4>
                            </div>
                            <div class="modal-body">
                                <form>
                                <input type="hidden" id="id" name="id">
                                    <div class="form-group"> 
                                        <label for="name" class="control-label">Nombre</label>
                                        <input type="text" class="form-control" id="name" name="full_name" required>
                                    </div>    

                                    <div class="form-group"> 
                                        <label for="state_id" class="control-label">Región</label>
                                        <select id="Regiones" class="form-control" required>
                                            
                                        </select>                    
                                    </div>                   
                                                            
                                    <div class="form-group"> 
                                        <label for="comuna_id" class="control-label">Comuna</label>
                                        <select class="form-control" id="comunas"required>
                                        </select>                    
                                    </div>   

                                    <div class="form-group"> 
                                        <label for="comunidad_id" class="control-label">Comunidad</label>
                                        <input type="text" class="form-control" id="comunidad" name="comunidad" required placeholder="Villa Antigua">
                                    </div>                                    
                                                            
                                    <div class="form-group"> 
                                        <label for="Longitud" class="control-label">Latitud</label>
                                        <input type="text" class="form-control" id="latitud" name="latitud" placeholder="Latitud">
                                        </select>                    
                                    </div>
                                    
                                    <div class="form-group"> 
                                        <label for="Longitud" class="control-label">Longitud</label>
                                        <input type="text" class="form-control" id="Longitud" name="Longitud" placeholder="Longitud">
                                        </select>                    
                                    </div>       
                                    
                                    <div class="form-group">
                                        <input type="button" id="btnGuardar" onclick="validarCoordenadas();" class="btn btn-primary" value="Guardar">
                                    </div>     
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="miModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content" style="width:125%;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Geolocalización</h4>
                            </div>
                            <div class="modal-body">
                                <div id="modalMapa"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>    
            <div class="div-tabla">
                  <table id="tablaApi"class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombres</th>
                            <th scope="col">ID</th>
                            <th scope="col">Región</th>
                            <th scope="col">Comunas</th>
                            <th scope="col">Comunidad</th>
                            <th scope="col">Latitud</th>
                            <th scope="col">Longitud</th>
                            <th scope="col">Geolocalizar</th>

                        </tr>

                    </thead>
                    <tbody id="table-contenido">
                        
                    </tbody>
                </table>
            </div>
            <div id="filtro">
                <hr>
                <button type="button" id="btnMesa" class="btn btn-info" data-toggle="modal" data-target="#miModal">
                    Ingresar Mesa de Trabajo
                </button>    
                <button id="btnExportar" class="btn btn-success" style="display: none;" onclick="fnExcelReport('tablaApi');">Exportar excel</button>
                <hr>
            </div> 
            <div class="div-tabla">
                <h4>Mesas de trabajos manuales</h4>
                  <table id="tablaBD" class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombres</th>
                            <th scope="col">ID</th>
                            <th scope="col">Región</th>
                            <th scope="col">Comunas</th>
                            <th scope="col">Comunidad</th>
                            <th scope="col">Latitud</th>
                            <th scope="col">Longitud</th>
                            <th scope="col">Geolocalizar</th>
                            <th scope="col">Acciones</th>
                        </tr>

                    </thead>
                    <tbody id="table-contenido2">
                    <?php
                        require_once 'CONTROLLER/Conexion.php';
                        $sql="SELECT * FROM comunidades WHERE estado=1";
                        $i=1;
                        foreach ($con->query($sql) as $row) {                        ?>
                            <tr>
                                <td> <?php echo $i?> </td>
                                <td> <?php echo $row['nombre']; ?></td>
                                <td> <?php echo $row['idcomunidades']; ?></td>
                                <td> <?php echo $row['region']; ?></td>
                                <td> <?php echo $row['comuna']; ?></td>
                                <td> <?php echo $row['comunidad']; ?></td>
                                <td> <?php echo $row['latitud']; ?></td>
                                <td> <?php echo $row['longitud']; ?></td>
                                <td><input id="btnMap" type="button" onclick="inicializar('<?php echo $row['latitud']; ?>','<?php echo $row['longitud']; ?>','<?php echo $row['comunidad']; ?>');" class="btn btn-info" data-toggle="modal" data-target="#miModal2" value="Ver Mapa"></td>
                                <td> <?php echo "<a title='Editar'><input data-toggle='modal' data-target='#miModal' type='button' id=" . $row['idcomunidades'] . " onclick='Javascript:PasarInformacion(this.id)' class='btn btn-warning'value='Editar'></a> <a><input type='button' id=" . $row['idcomunidades'] . " onclick='Javascript:Eliminar(this.id)' title='Eliminar' class='btn btn-danger'value='Eliminar'></i></a>" ?></td>
                            </tr>
                    <?php $i++;} ?>
                    </tbody>
                </table>
            </div>
            
            <div id="filtro">
                <hr>    
                <button id="btnExportar" class="btn btn-success" onclick="fnExcelReport('tablaBD');">Exportar excel</button>
                <hr>
            </div>
            <footer>2020 Verónica Escobar A.</footer>    
        </body>
</html>